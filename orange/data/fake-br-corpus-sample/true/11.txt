Anthony Garotinho Ã© ouvido mais uma vez sobre suposta agressÃ£o na Cadeia de Benfica

Delegado vai pedir autorizaÃ§Ã£o para que Anthony Garotinho possa sair de Bangu para prestar um novo depoimento na Cidade da PolÃ­cia e fazer o retrato falado do suposto agressor.

ex-governador Anthony Garotinho prestou novo depoimento sobre a suposta agressÃ£o que sofreu na Cadeia PÃºblica JosÃ© Frederico Marques, em Benfica, na Zona Norte do Rio. Os agentes o ouviram no Complexo PenitenciÃ¡rio de GericinÃ³, em Bangu, para onde ele foi transferido.

O depoimento durou cerca de duas horas. Garotinho afirmou que, no dia em que ele soube que iria ser transferido para a ala B da cadeia de Benfica, para ficar separado dos outros presos da Lava Jato, ele teria recebido um bilhete onde estava escrito: âIsto vai te fazer bemâ.

Durante o depoimento, o ex-governador afirmou que nÃ£o entendeu muito bem o conteÃºdo da mensagem, mas na mesma noite em que recebeu o bilhete, ele teria sofrido a agressÃ£o.

âPara que nÃ£o paire nenhuma dÃºvida, vamos tentar ouvir os detentos da galeria A, para saber se eles ouviram alguma coisa e conseguem determinar o que ouviram, se conseguem identificar ou nÃ£oâ, explicou o delegado Carlos CÃ©sar Santos.

O delegado vai pedir autorizaÃ§Ã£o para que Anthony Garotinho possa sair de Bangu para prestar um novo depoimento na Cidade da PolÃ­cia e fazer o retrato falado do suposto agressor.

O retrato falado nÃ£o foi realizado no depoimento em Bangu por falta de condiÃ§Ãµes tÃ©cnicas.

Garotinho jÃ¡ tinha prestado um outro depoimento onde estÃ¡ preso. Na ocasiÃ£o, o retrato falado do suposto agressor tambÃ©m nÃ£o foi realizado por um problema em um computador.

Rosinha Matheus, esposa de Anthony Garotinho e tambÃ©m ex-governadora, foi solta na quinta-feira (30) por uma decisÃ£o dos desembargadores do Tribunal Regional Eleitoral.

